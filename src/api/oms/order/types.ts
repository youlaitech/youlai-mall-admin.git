/**
 * 订单查询参数
 */
export interface OrderQuery extends PageQuery {
  orderSn: string | undefined;
  status: number | undefined;
}

/**
 * 订单分页列表项
 */
export interface Order {
    id: string;
    orderSn: string;
    totalAmount: string;
    payAmount: string;
    payType: PayTypeStatus;
    status: OrderStatus;
    totalQuantity: number;
    createTime: string;
    memberId: string;
    sourceType: OrderSourceStatus;
    orderItems: OrderItem[];
    remark:string;
    couponId:number;
    couponAmount:number;
    freightAmount:number;
    payTime:string;
    outTradeNo:string;
    transactionId:string;
    outRefundNo:string;
    refundId:string;
    deliveryTime:string;
    receiveTime:string;
    commentTime:string;
    deleted:number;
}
export interface OrderItem {
  id: string;
  orderId: string;
  skuId: string;
  skuName: string;
  picUrl: string;
  price: string;
  count: number;
  totalAmount: number;
}

/**
 * 订单分页
 */
export type OrderPageResult = PageResult<Order[]>;

/**
 * 订单表单
 */
export interface OrderDetail {
    order: Order;
    orderItems: OrderItem[];
    member: MemberRegister;
}

/**
 * 会员
 */
export interface MemberRegister {
    gender: number;
    nickName: string;
    mobile: string;
    birthday: string;
    avatarUrl: string;
    openid: string;
    sessionKey: string;
    city: string;
    country: string;
    language: string;
    province: string;
}

/**
 * 订单状态枚举
 */
export enum OrderStatusEnum {
    /**
     * 待付款
     */
    WAIT_PAY = 1,
    /**
     * 待发货
     */
    WAIT_SHIPPING = 2,
    /**
     * 已发货
     */
    SHIPPED = 3,

    /**
     * 已完成
     */
    FINISHED = 4,
    /**
     * 已关闭
     */
    CLOSED = 5,
    /**
     * 已取消
     */
    CANCELED = 6,
}
/**
 * 订单来源
 */
export type OrderSourceStatus = 1 | 2 | 3;
export type OrderSourceMap = Record<OrderSourceStatus, string>;
/**
 * 支付方式
 */
export type PayTypeStatus = 1 | 2 | 3;
export type PayTypeMap = Record<PayTypeStatus, string>;
/**
 * 订单状态
 */
export type OrderStatus = 0 | 1 | 2 | 3 | 4 | 5;
export type OrderStatusMap = Record<OrderStatus, string>;
